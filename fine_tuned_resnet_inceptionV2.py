from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.applications.inception_resnet_v2 import preprocess_input, decode_predictions
from keras.layers import Dense, Input
from keras.preprocessing import image
from keras.models import load_model
from keras.utils import np_utils
import numpy as np
import glob
from keras.models import *

labels = ['car','train','lorry','bus']
inputs = glob.glob('./*.jpg')

output = [0,0,1,1,1,1,1,1,1,1,1,0,1,2,2,2,2,2,2,2,2,2,0,2,3,3,3,3,3,3,3,3,0,3,0,0,0,0]

x_train =[]

for photo in inputs:
        img = image.load_img(photo, target_size=(299, 299))
        tr_x = image.img_to_array(img)
        tr_x = preprocess_input(tr_x)
        x_train.append(tr_x)
        

x_train = np.array(x_train)
y_train = np.array(output)

y_train = np_utils.to_categorical(y_train)
input = Input(shape=(299, 299, 3))

base_model = InceptionResNetV2(include_top=False, weights='imagenet', input_tensor=input, input_shape=(299, 299, 3), pooling='avg', classes=1000)

for l in base_model.layers:
        l.trainable = False
t = base_model(input)
o = Dense(len(labels), activation='softmax')(t)
model = Model(inputs=input, outputs=o)

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])

model.fit(x_train, y_train,
                  batch_size=32,
                  epochs=10,
                  shuffle=True,
                  verbose=1
                  )

model.save("tuned_resnet_inceptionV2.h5")


img = image.load_img('39.png', target_size=(299, 299))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

preds = model.predict(x,batch_size=None, verbose=0)
        